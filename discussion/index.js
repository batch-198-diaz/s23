// index.js

// JS OBJECTS

/* 
JS Objects is a way to organize data with context through the use of key-value pairs and also a way to define a real world object with its own characteristics. Objects are composed of key-value pairs. Keys provide labels to your values. 

	Syntax

	key: value

*/



/* array versus object
	
	- Use array to create a list
	- Use object to describe something with characteristics
*/


let dog_array = ["loyal",4,"tail","Husky"] 

let dog_obj = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true

}

/*

	Mini-Activity
	
	Create an object that describes your favourite movie/vidya game
	-title
	-publisher
	-year
	-price
	-isAvailable

	log your object in the console

*/

let fave_game = {

	title: "Timberborn",
	publisher: "Mechanistry",
	year: 2021,
	price: 549.95,
	isAvailable: true
	
};

console.log(fave_game);


// Accessing Object properties = objectName.propertyName

console.log(fave_game.title);
console.log(fave_game.publisher);

// Updating values

fave_game.title = "Valheim";
fave_game.publisher = "Coffee Stain Publishing";
fave_game.price = 449.95;

console.log(fave_game);
console.log(fave_game.title);
console.log(fave_game.publisher);

// Accessing array items inside an object

let course = {

	title: "Philosophy 101",
	description: "Learn the value of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson","Mrs. Smith","Mr. Francis"]
};

console.log(course);
console.log(course.instructors);
console.log(course.instructors[1]);

course.instructors.pop();
console.log(course);

/* 
	Mini-Activity

	1. Add a new instructor to our course named "Mr. McGee"
		-log the instructors array in the console.

	2. Determine if the course has an instructor called "Mr. Johnson"
		-use only an array method.
		-save the result of the array method in a variable.
		-log the variable in the console.
		-output should be a boolean.

*/


course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isThere = course.instructors.includes("Mr. Johnson");
console.log(isThere);

//-create a function that is able to keep adding new instructors to our object

function addNewInstructor(instructor){

	course.instructors.push(instructor);
}
addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");

console.log(course);


/*

	Mini-Activity
	
	1. Add an if-else statement
		- if the instructor is already in the instructors array, show a console.log() message:
			"Instructor already added."
		- else add the instructor in the array and show a console.log() message:
			"Thank you. Instructor added."
	2. You can use an array method that determines if an item is in the array
	3. Add two new instructors of the same name and take a screenshot of your console.

*/

function addNewInstructor_2(instructor){

	/* if(course.instructors.includes(instructor)===true){ 
		NOTE: you can omit condition "===true" because boolean only have 2 values true and false so if you include it in an if-else statement, it's understood that the only two choices would be applied to one of each condition.*/
	if(course.instructors.includes(instructor)){ 
		console.log("Instructor already added.")
	} else {
		course.instructors.push(instructor);
		console.log("Thank you. Instructor added.");
	}
	
}

addNewInstructor_2("Ms. Jackson"); //result: Thank you. Instructor added.
addNewInstructor_2("Ms. Jackson"); //result: Instructor already added.
console.log(course.instructors);

// Create/Initialize an EMPTY OBJECT and then add its properties afterwards

let instructor = {};
console.log(instructor);

instructor.name = "James Johnson";
console.log(instructor);

/*

	Mini-Activity
	Populate the instructor object with properties/characteristics that define/describe our instructor:

	James Johnson is an instructor. He is 56 years old and his gender is male. His department is Humanities and his current salary is 50,000. He teaches Philosophy, Humanities and Logic.
*/

instructor.age = 56;
instructor.gender = "Male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.courses = ["Philosophy","Humanities","Logic"];

console.log(instructor);


// Accessing object within an object

instructor.address = {

	street: '#1 Maginhawa St.',
	city: 'Quezon City',
	country: 'Philippines'

}

console.log(instructor);
console.log(instructor.address.street);

/* 
Create Objects using a Constructor Function
	-create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object.
	-"this" keyword when added in a constructor function refers to the object that will be made by the function
*/

function Superhero(name,superpower,powerLevel){

	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;
}

/* 
Create an object out of our superhero constructor function
new keyword is added to allow us to create a new object out of our function.
*/

let superhero1 = new Superhero("Saitama","One Punch",30000);
console.log(superhero1);

/*
	Mini-Activity
	
	Create a constructor function able to receive 3 arguments
	- it should be able to receive 2 strings and a number
	- using "this" keyword assign properties:
		name
		brand
		price
	- assign the parameters as values to each property

	Create 2 new objects called laptop1 and laptop2 using our contructor

*/

function Laptop(name,brand,price){

	this.name = name;
	this.brand = brand;
	this.price = price;
}

let laptop1 = new Laptop("Acer","Predator Helios 300",36000);
let laptop2 = new Laptop("ASUS","ROG Zephyrus G14",41000);
console.log(laptop1);
console.log(laptop2);


// Object Methods

/*
	are functions associated as a property of an object and these methods are anonymous functions we can invoke using the property of the object.
*/

let person = {
	name: "Slim Shady",
	talk: function(){
		console.log("Hi! My name is, What? My name is who? " + this.name);
	}

}

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){
		console.log(friend);
	}
}

let person3 = {
	name: "Khaled",
	call: function(friend){
		console.log("Good day, " + friend.name);
	}
}

person.talk();
person2.greet(person);
person3.call(person);


// Create a constructor with a built-in method

function Dog(name,breed){

/*
	{
		name: <valueParameterName>,
		breed: <valueParameterBreed>
	}
*/

	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}
}

let dog1 = new Dog("Rocky","Bulldog");
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Blackie","Rottweiler");
console.log(dog2);