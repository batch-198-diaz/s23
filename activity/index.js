// index.js - activity


// TRAINER INFO 

let trainer = {}

console.log(trainer);

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu","Charizard","Squirtle","Bulbasaur"];
trainer.friends = {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	};

trainer.talk = function(){
		console.log("Pikachu! I choose you!");
	};

console.log(trainer);

console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of bracket notation:")
console.log(trainer.pokemon);
console.log("Result of talk method:")
trainer.talk();



// CONSTRUCTOR FUNCTION

function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = 3*level;
	this.attack = 1.5*level;


	this.faint = function(){
		console.log(name + " has fainted!");
	}

	this.tackle = function(pokemon){

		pokemon.health = (pokemon.health - (1.5*level));
		console.log(name + " tackled " + pokemon.name);

		if(pokemon.health <= 0){
			console.log(pokemon.name + "'s remaining HP: " + pokemon.health);
			pokemon.faint();
		} else {
			console.log(pokemon.name + "'s remaining HP: " + pokemon.health);
		}
	}

}

// CHOOSE POKEMON

let pokemon1 = new Pokemon("Pikachu",12);
let pokemon2 = new Pokemon("Geodude",8);
let pokemon3 = new Pokemon("Mewtwo",100);

// POKEMON INFO

console.log(pokemon1)
console.log(pokemon2)
console.log(pokemon3)


// POKEMON FIGHT!!!

pokemon1.tackle(pokemon2);
pokemon1.tackle(pokemon2);
pokemon3.tackle(pokemon1);




// pokemon3.tackle(pokemon2);
// pokemon1.health = (pokemon1.health - pokemon2.attack);
// console.log(pokemon1.name + "s health is now reduced to " + pokemon1.health);
// console.log(pokemon1);

// pokemon3.tackle(pokemon2);
// pokemon2.health = (pokemon2.health - pokemon3.attack);
// console.log(pokemon2.name + "s health is now reduced to " + pokemon2.health);
// pokemon2.faint();
// console.log(pokemon2);





